/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oop_assignment;

import com.google.gson.Gson; 
import com.google.gson.GsonBuilder;  
import com.google.gson.reflect.TypeToken;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class Json {
    
    public static void main(String[] args) throws IOException {
        
    Administrator admin1 = new Administrator("administrator","buCe2UsHuH");
    
    ArrayList<Administrator> list = new ArrayList<Administrator>();
        list.add(admin1);
        
        Gson gson = new GsonBuilder().create();
        
        String jsonString = gson.toJson(admin1);
        System.out.println(jsonString);
        
        Administrator sObj = gson.fromJson(jsonString, Administrator.class);
        System.out.println(sObj.username);
        
        jsonString = gson.toJson(list);
        System.out.println(jsonString);
        
        Type listType = new TypeToken<ArrayList<Administrator>>(){}.getType();
        ArrayList<Administrator> sArray = gson.fromJson(jsonString, listType);  
       
        try (Writer writer = new FileWriter("adminac.json")) {
            gson.toJson(list, writer);
        } catch (Exception e) {
            e.printStackTrace();
        } 
        

    }
        }

